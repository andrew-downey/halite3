#include "hlt/constants.hpp"
#include "hlt/game.hpp"
#include "hlt/log.hpp"

#include <chrono>
#include <ctime>
#include <fstream>
#include <random>

using namespace std;
using namespace hlt;

const string botName = "Anoobot8";
map<string, float> tunables;

Command getBestCell(unique_ptr<GameMap>& game_map, shared_ptr<Ship> ship, int turnNumber)
{
    Position target = ship->position;
    // float gainedIfStay = 0.1 * haliteUnderShip;
    // float costToMove = game_map->at(target)->halite * 0.1;

    const int sightRange = tunables["SHIP_SIGHT_RANGE"];
	float bestCellValue = 0;
    for (int y = ship->position.y - sightRange; y <= ship->position.y + sightRange; y++) {
        for (int x = ship->position.x - sightRange; x <= ship->position.x + sightRange; x++) {
            Position pos = Position(x, y);
            MapCell* cell = game_map->at(pos);
            const int distance = game_map->calculate_distance(ship->position, cell->position);

            if (distance > sightRange || distance == 0) {
                continue;
            }

            // float gainedIfMove = (cell->halite * 0.25) - (0.1 * haliteUnderShip);
            float newCellValue = cell->halite / (distance * tunables["DISTANCE_SCALAR"]);
            if (newCellValue >= bestCellValue) { // && gainedIfMove > gainedIfStay) {
				bestCellValue = newCellValue;
                target = pos;
            }
        }
    }

    Command command = ship->move(Direction::STILL);
    if (target != ship->position) {
        log::log("[Ship " + to_string(ship->id) + "][Think] Targeting cell [" + to_string(target.x) + "][" + to_string(target.y) + "]");
        Direction direction = game_map->navigate(ship, target, turnNumber);
        command = ship->move(direction);
    }

    return command;
}

Command findNearestDropoff(unique_ptr<GameMap>& game_map, shared_ptr<Player> me, shared_ptr<Ship> ship, int turnNumber)
{
    log::log("[Ship " + to_string(ship->id) + "][Think] Finding nearest dropoff");
    Position target = me->shipyard->position;

    for (auto mapCell : me->dropoffs) {
        shared_ptr<Entity> dropoff = mapCell.second;
        if (game_map->calculate_distance(ship->position, dropoff->position) < game_map->calculate_distance(ship->position, target))
            target = dropoff->position;
    }

    const int distance = game_map->calculate_distance(ship->position, target);
    log::log("[Ship " + to_string(ship->id) + "][Think] Targeting dropoff at [" + to_string(target.x) + "][" + to_string(target.y) + "] (distance " + to_string(distance) + ")");

    Command command = ship->move(Direction::STILL);
    if (target != ship->position) {
        log::log("[Ship " + to_string(ship->id) + "][Think] Targeting cell [" + to_string(target.x) + "][" + to_string(target.y) + "]");
        Direction direction = game_map->navigate(ship, target, turnNumber);
        command = ship->move(direction);
    }
    if (turnNumber < tunables["DROPOFF_SPAWN_TURNS"] && (me->halite + ship->halite + game_map->at(ship)->halite) > constants::DROPOFF_COST && game_map->calculate_distance(ship->position, target) > tunables["DROPOFF_MIN_DISTANCE"]) {
        log::log("[Ship " + to_string(ship->id) + "][Think] Converting to Dropoff (Not Actually)");
        command = ship->make_dropoff();
        me->halite -= constants::DROPOFF_COST;
    }

    return command;
}

int main(int argc, char* argv[])
{
    unsigned int rng_seed;
    if (argc > 1) {
        rng_seed = static_cast<unsigned int>(stoul(argv[1]));
    } else {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }
    mt19937 rng(rng_seed);

    tunables["SHIP_MAP_SIGHT_RATIO"] = 0.5;
    tunables["SHIPS_TURN_SPAWN_RATIO"] = 0.6;
    tunables["DROPOFF_TURN_SPAWN_RATIO"] = 0.6;
    tunables["SHIP_MAP_RATIO"] = 0.5;
    tunables["DROPOFF_DISTANCE_RATIO"] = 0.4;
    tunables["HALITE_RETURN_THRESH"] = 800;
    tunables["SHIP_SPAWN_THRESH"] = 1001;
    tunables["CELL_HALITE_EMPTY"] = 30;
    tunables["DISTANCE_SCALAR"] = 0.2;

    Game game;

    // Derivatives
    tunables["SHIP_SIGHT_RANGE"] = game.game_map->height * tunables["SHIP_MAP_SIGHT_RATIO"];
    tunables["SHIPS_MAX"] = game.game_map->height * tunables["SHIP_MAP_RATIO"];
    tunables["DROPOFF_MIN_DISTANCE"] = game.game_map->height * tunables["DROPOFF_DISTANCE_RATIO"];
    tunables["DROPOFF_SPAWN_TURNS"] = constants::MAX_TURNS * tunables["DROPOFF_TURN_SPAWN_RATIO"];
    tunables["SHIP_SPAWN_TURNS"] = constants::MAX_TURNS * tunables["SHIPS_TURN_SPAWN_RATIO"];
    tunables["ENDGAME_TURNS"] = game.game_map->height * 0.5;
    tunables["ENDGAME_TURNS_START"] = constants::MAX_TURNS - tunables["ENDGAME_TURNS"];

    log::log("[Game] My Player ID is " + to_string(game.my_id) + ". Bot rng seed is " + to_string(rng_seed) + ".");
    for (auto tunable = tunables.begin(); tunable != tunables.end(); tunable++) {
        log::log(tunable->first + ": " + to_string(tunable->second));
    }
    log::log("");

    // As soon as you call "ready" function below, the 2 second
    // per turn timer will start.
    game.ready(botName);

    for (;;) {
        auto start = std::chrono::system_clock::now();

        game.update_frame();
        shared_ptr<Player> me = game.me;
        unique_ptr<GameMap>& game_map = game.game_map;

        vector<Command> command_queue;

        for (const auto& ship_iterator : me->ships) {
            shared_ptr<Ship> ship = ship_iterator.second;
            Command command = ship->move(Direction::STILL);

            // Can we even move?
            if (ship->halite >= game_map->at(ship->position)->halite * 0.1) {
                const float returnThresh = tunables["HALITE_RETURN_THRESH"];
                const int endgameTurns = tunables["ENDGAME_TURNS_START"];
                if (!ship->is_full() && ship->halite < returnThresh && game.turn_number < endgameTurns) {
                    log::log("[Ship " + to_string(ship->id) + "][Think] Gathering Halite");
                    if (game_map->at(ship)->halite < tunables["CELL_HALITE_EMPTY"] && (ship->halite + (game_map->at(ship)->halite * 0.25) < tunables["HALITE_RETURN_THRESH"])) {
                        command = getBestCell(game_map, ship, game.turn_number);
                    }
                } else {
                    log::log("[Ship " + to_string(ship->id) + "][Think] Finding nearest dropoff");
                    command = findNearestDropoff(game_map, me, ship, game.turn_number);
                }
            }
            command_queue.push_back(command);
        }

        const int shipSpawnTurns = tunables["SHIP_SPAWN_TURNS"];
        const float shipSpawnThresh = tunables["SHIP_SPAWN_THRESH"];
        if (game.turn_number <= shipSpawnTurns && me->ships.size() < tunables["SHIPS_MAX"] && me->halite >= shipSpawnThresh && !game_map->at(me->shipyard)->is_occupied()) {
            log::log("[Shipyard] Spawning Ship");
            command_queue.push_back(me->shipyard->spawn());
        }

        log::log("[Game] Time this frame (ms): " + to_string(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count()));

        if (!game.end_turn(command_queue)) {
            break;
        }
    }

    return 0;
}
