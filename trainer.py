#!/usr/bin/env python


import subprocess
import string
import json
import threading
import multiprocessing
import time


class Tournament(threading.Thread):
    def __init__(self, commands, results, numMatches=1):
        threading.Thread.__init__(self)
        self.commands = commands
        self.results = results
        self.numMatches = numMatches

    def run(self):
        for _ in range(self.numMatches):
            runMatch(self.commands, self.results)


bots = ['MyBot.exe', 'Anoobot6.exe']


def runMatch(commands, results):
    matchResults = subprocess.check_output(commands)
    matchJson = json.loads(matchResults)
    results.append(matchJson)


def main():
    commands = [
        'halite.exe',
        '--replay-directory replays/',
        '--results-as-json'
    ]

    for bot in bots:
        commands.append("\"" + bot + "\"")

    totalResults = {}
    for bot in bots:
        totalResults[bot] = BotResult(bot)

    results = []
    tournaments = []
    numTournaments = 10
    numMatches = 2
    for _ in range(numTournaments):
        tournament = Tournament(commands, results, numMatches)
        time.sleep(1)
        tournament.start()
        tournaments.append(tournament)

    for tournament in tournaments:
        tournament.join()

    for result in results:
        for bot in result['stats']:
            botStats = result['stats'][bot]
            if(int(bot) == 0):
                if(botStats['rank'] != 1):
                    print("MyBot lost this match: " + result['replay'])
                if(botStats['score'] <= 4000):
                    print(
                        "[CRITICAL] MyBot had a terrible score in this match: " + result['replay'])

            resultObj = totalResults[bots[int(bot)]]
            rank = botStats['rank']
            if(rank == 1):
                resultObj.wins += 1
            else:
                resultObj.losses += 1
            resultObj.scores.append(botStats['score'])

    for result in totalResults:
        bot = totalResults[result]
        print("{} has {} wins ({}%) and a score spread of {}-{} ({})".format(
            bot.name, bot.wins, bot.winRate, min(bot.scores), max(bot.scores), bot.averageScore))

    return


class BotResult:
    name = ""
    wins = 0
    losses = 0

    def __init__(self, name):
        self.name = name
        self.scores = []

    @property
    def winRate(self):
        return int(self.wins / (self.losses + self.wins) * 100)

    @property
    def averageScore(self):
        return int(sum(self.scores) / float(len(self.scores)))


if __name__ == '__main__':
    main()
