#pragma once

#include "map_cell.hpp"
#include "types.hpp"

#include <climits>
#include <map>
#include <unordered_map>
#include <vector>

extern std::map<std::string, float> tunables;

namespace hlt {
struct GameMap {
    int width;
    int height;
    std::vector<std::vector<MapCell>> cells;

    // a structure to represent a weighted edges in graph
    struct Edge {
        Position source, dest;
        int weight;

        Edge(Position source, Position dest, int weight)
            : source(source)
            , dest(dest)
            , weight(weight)
        {
        }
    };

    // a structure to represent a connected, directed and
    // weighted graph
    struct Graph {
        // V-> Number of vertices, E-> Number of edges
        int V;

        // graph is represented as an array of edges.
        std::vector<Edge>* edges;
    };

    // Creates a graph with V vertices and E edges
    struct Graph* createGraph(int V)
    {
        struct Graph* graph = new Graph;
        graph->V = V;
        graph->edges = new std::vector<Edge>();
        return graph;
    }

    MapCell* at(const Position& position)
    {
        Position normalized = normalise(position);
        return &cells[normalized.y][normalized.x];
    }

    MapCell* at(const Entity& entity)
    {
        return at(entity.position);
    }

    MapCell* at(const Entity* entity)
    {
        return at(entity->position);
    }

    MapCell* at(const std::shared_ptr<Entity>& entity)
    {
        return at(entity->position);
    }

    int calculate_distance(const Position& source, const Position& target)
    {
        const auto& normalised_source = normalise(source);
        const auto& normalized_target = normalise(target);

        const int dx = std::abs(normalised_source.x - normalized_target.x);
        const int dy = std::abs(normalised_source.y - normalized_target.y);

        const int toroidal_dx = std::min(dx, width - dx);
        const int toroidal_dy = std::min(dy, height - dy);

        return toroidal_dx + toroidal_dy;
    }

    Position normalise(const Position& position)
    {
        const int x = ((position.x % width) + width) % width;
        const int y = ((position.y % height) + height) % height;
        return { x, y };
    }

    std::vector<Direction> get_unsafe_moves(const Position& source, const Position& destination)
    {
        const auto& normalised_source = normalise(source);
        const auto& normalised_destination = normalise(destination);

        const int dx = std::abs(normalised_source.x - normalised_destination.x);
        const int dy = std::abs(normalised_source.y - normalised_destination.y);
        const int wrapped_dx = width - dx;
        const int wrapped_dy = height - dy;

        std::vector<Direction> possible_moves;

        if (normalised_source.x < normalised_destination.x) {
            possible_moves.push_back(dx > wrapped_dx ? Direction::WEST : Direction::EAST);
        } else if (normalised_source.x > normalised_destination.x) {
            possible_moves.push_back(dx < wrapped_dx ? Direction::WEST : Direction::EAST);
        }

        if (normalised_source.y < normalised_destination.y) {
            possible_moves.push_back(dy > wrapped_dy ? Direction::NORTH : Direction::SOUTH);
        } else if (normalised_source.y > normalised_destination.y) {
            possible_moves.push_back(dy < wrapped_dy ? Direction::NORTH : Direction::SOUTH);
        }

        return possible_moves;
    }

    // Bellman Ford pathfinding
    std::vector<Direction> getBFMoves(std::shared_ptr<Ship> ship, const Position& destination, const int turnNumber)
    {
        Position source = ship->position;
        // const auto& normalised_source = normalise(source);
        // const auto& normalised_destination = normalise(destination);

        const int PADDING = 3;

        /* Let us create the graph given in above example */
        int height = 2 * PADDING + abs(source.x - destination.x);
        int width = 2 * PADDING + abs(source.y - destination.y);
        int V = height * width; // Number of vertices in graph
        struct Graph* graph = createGraph(V);

        log::log("[Ship " + std::to_string(ship->id) + "][Path] Finding moves for [" + std::to_string(source.x) + "][" + std::to_string(source.y) + "] to [" + std::to_string(destination.x) + "][" + std::to_string(destination.y) + "]");
        for (int x = std::min(source.x, destination.x) - PADDING; x <= std::max(source.x, destination.x) + PADDING; x++) {
            for (int y = std::min(source.y, destination.y) - PADDING; y <= std::max(source.y, destination.y) + PADDING; y++) {
                Position currentCell = Position(x, y);

                const std::vector<Position> directions = {
                    { 0, 1 },
                    { 1, 0 },
                    { 0, -1 },
                    { -1, 0 },
                };

                for (auto direction : directions) {
                    auto directedPosition = Position(x + direction.x, y + direction.y);
                    int weight = 999999;
                    if (!at(directedPosition)->is_occupied()) {
                        weight = at(source)->halite;
                    }

                    graph->edges->push_back(Edge(currentCell, directedPosition, weight));
                }
            }
        }

        std::unordered_map<Position, int> dist = std::unordered_map<Position, int>();
        std::unordered_map<Position, Position> predecessors = std::unordered_map<Position, Position>();

        // Step 1: Initialize distances from source to all other vertices
        // as INFINITE
        for (auto edge = graph->edges->begin(); edge != graph->edges->end(); edge++) {
            dist[edge->dest] = INT_MAX;
        }
        dist[source] = 0;

        // Step 2: Relax all edges |V| - 1 times. A simple shortest
        // path from source to any other vertex can have at-most |V| - 1
        // edges
        log::log("[Ship " + std::to_string(ship->id) + "][Path] Relaxing " + std::to_string(graph->edges->size()) + " edges " + std::to_string(V) + " times");
        for (int relax = 0; relax <= V; relax++) {
            for (auto edge = graph->edges->begin(); edge != graph->edges->end(); edge++) {
                Position u = edge->source;
                Position v = edge->dest;
                int weight = edge->weight;

                if (dist.find(u)->second != INT_MAX && dist.find(u)->second + weight < dist.find(v)->second) {
                    dist.find(v)->second = dist.find(u)->second + weight;
                    predecessors.insert_or_assign(edge->dest, edge->source);
                }
            }
        }

        // Step 3: check for negative-weight cycles.  The above step
        // guarantees shortest distances if graph doesn't contain
        // negative weight cycle.  If we get a shorter path, then there
        // is a cycle.
        for (auto edge = graph->edges->begin(); edge != graph->edges->end(); edge++) {
            Position u = edge->source;
            Position v = edge->dest;
            int weight = edge->weight;
            if (dist.find(u)->second != INT_MAX && dist.find(u)->second + weight < dist.find(v)->second) {
                log::log("[Ship " + std::to_string(ship->id) + "][Path][ERROR] Graph contains negative weight cycle");
                for (auto edge = graph->edges->begin(); edge != graph->edges->end(); edge++) {
                    log::log("[" + std::to_string(edge->source.x) + "][" + std::to_string(edge->source.y) + "]-[" + std::to_string(edge->dest.x) + "][" + std::to_string(edge->dest.y) + "]				" + std::to_string(edge->weight));
                }
                break;
            }
        }

        unsigned int safety = 0;
        auto pair = predecessors.find(destination);
        while (pair->second != source) {
            if (safety++ >= predecessors.size()) {
                log::log("[Ship " + std::to_string(ship->id) + "][Path][ERROR] Hit safety in predecessors");
                break;
            }

            // log::log("[Ship " + std::to_string(ship->id) + "][Path] Predecessor [" + std::to_string(pair->first.x) + "][" + std::to_string(pair->first.y) + "] from [" + std::to_string(pair->second.x) + "][" + std::to_string(pair->second.y) + "]");
            pair = predecessors.find(pair->second);

            if (pair == predecessors.end()) {
                log::log("[Ship " + std::to_string(ship->id) + "][Path][ERROR] Predecessor not found");
                break;
            }
        }

		Position target = pair->first;
        if (at(destination)->has_structure() && predecessors.size() == 1) {
            log::log("About to move onto a dropoff");
			target = Position(pair->first.x, pair->first.y - 1);
        }

        std::vector<Direction> moves = std::vector<Direction>();

        if (turnNumber < tunables["ENDGAME_TURNS_START"]) {
            if (at(target)->is_occupied()) {
                moves.push_back(Direction::STILL);
            } else {
                moves = get_unsafe_moves(source, target);
            }
        } else {
            if (at(target)->is_occupied() && !at(target)->has_structure()) {
                moves.push_back(Direction::STILL);
            } else {
                moves = get_unsafe_moves(source, target);
            }
        }

        return moves;
    }

    Direction navigate(std::shared_ptr<Ship> ship, const Position& destination, const int turnNumber)
    {
        auto moves = getBFMoves(ship, destination, turnNumber);

        for (auto direction : moves) {
            log::log("[Ship " + std::to_string(ship->id) + "][Decision] Considering moving " + static_cast<char>(direction));
            Position target_pos = ship->position.directional_offset(direction);
            if (at(ship)->halite == 0 || ship->halite > at(ship)->halite * 0.1) {
                log::log("[Ship " + std::to_string(ship->id) + "][Decision] Enough halite to move");
                at(target_pos)->mark_unsafe(ship);
                return direction;
            } else {
                log::log("[Ship " + std::to_string(ship->id) + "][Decision][ERROR] Not enough halite to move");
            }
        }

        return Direction::STILL;
    }

    void _update();
    static std::unique_ptr<GameMap> _generate();
};
}